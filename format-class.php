<?php 
/**
* Format class
*/
class Format{
	// Function for converting time to ago
	public function makeAgo($str){ 
		// Converting date and time from date here

   		list($date, $time) = explode(' ', $str);
    	list($year, $month, $day) = explode('-', $date);
    	list($hour, $minute, $second) = explode(':', $time);
    	$timestamp = mktime($hour, $minute, $second, $month, $day, $year);

		// Make ago from converting date time here 
   		$difference = time() - $timestamp;
   		$periods = array("sec", "min", "hr", "day", "week", "month", "year", "fourteenth"); //ninteenth = 90
   		$lengths = array("60","60","24","7","4.35","12","40");
   		for($i = 0; $difference >= $lengths[$i]; $i++){
   			$difference /= $lengths[$i];
		}
   		$difference = round($difference);

   		if($difference != 1){ $periods[$i].= "s";}
   		$text = "$difference $periods[$i] ago";
   		return $text;
	}

}

// close format class from here...
echo "<br/><hr>";

// Query your database here and get timestamp

$sqldate = '1978-02-18 11:34:10';

// create format class object below...

$obj->Format();

echo $obj-> makeAgo($sqldate); //Convert Date Time Then convert to ago time


	
?>

